//
//  RectangleAnimation.swift
//  SwiftUIAnimation
//
//  Created by Florian PICHON on 27/01/2022.
//

import SwiftUI

struct RectangleAnimation: View {
    @State private var isLoading = false
    private let animation = Animation.linear(duration: 1).repeatForever(autoreverses: false)
    var body: some View {
        ZStack{
            Text("Loading")
                .font(.system(.body, design: .rounded))
                .bold()
                .offset(x:0, y:-25)
            RoundedRectangle(cornerRadius: 3)
                .stroke(Color(.systemGray5), lineWidth: 3)
                .frame(width:250, height: 3)
            RoundedRectangle(cornerRadius: 3)
                .stroke(Color.green, lineWidth: 3)
                .frame(width: 30, height: 3)
                .offset(x: isLoading ? 100 : -100, y:0)
        }
        .onAppear(){
            withAnimation(self.animation){
                self.isLoading = true
            }
        }
    }
}

struct RectangleAnimation_Previews: PreviewProvider {
    static var previews: some View {
        RectangleAnimation()
    }
}
