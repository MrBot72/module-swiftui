//
//  RotationEffectCircle.swift
//  SwiftUIAnimation
//
//  Created by Florian PICHON on 27/01/2022.
//

import SwiftUI

struct RotationEffectCircle: View {
    @State private var isChanged = false
    private let animation = Animation.easeInOut(duration: 1).repeatForever(autoreverses: false)
    var body: some View {
        ZStack {
            Circle()
                .stroke(Color(.systemGray5), lineWidth: 14)
                .frame(width:100, height: 100)
            Circle()
                .trim(from: 0, to: 0.2)
                .stroke(Color.green, lineWidth: 7)
                .frame(width: 100, height: 100)
                .rotationEffect(Angle(degrees: isChanged ? 360 : 0))
                .onAppear(){
                    withAnimation(self.animation){
                        self.isChanged = true
                    }
            }
        }
    }
}

struct RotationEffectCircle_Previews: PreviewProvider {
    static var previews: some View {
        RotationEffectCircle()
    }
}
