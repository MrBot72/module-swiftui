//
//  SwiftUIFormApp.swift
//  SwiftUIForm
//
//  Created by Florian PICHON on 31/01/2022
//

import SwiftUI

@main
struct SwiftUIFormApp: App {
    var settingStore = SettingStore()
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(settingStore)
        }
    }
}
